---
title: "Why TFstate Cloud"
date: 2021-10-29T11:33:50+03:00
draft: false
weight: 10
showtoc: true
TocOpen: true

---
## TFstate Common Use Cases
###  Terraform in CI/CD
Use TF State Cloud to run Terraform in CI/CD pipelines, and preserve the state across pipeline executions.

### Terraform in Kubernetes
Use TF State Cloud to run Terraform in Kubernetes. The Kubernetes pods are stateless by nature. When running Terraform in Kubernetes, the state and locks are created locally in the container ephemeral storage, and thus not preserved. TFstate.Cloud allows keeping the state across container executions.

### Multiple machines
If you are using multiple machines to run Terraform, you can use TFstate.cloud to share the infrastructure state across the machines.

### Teams
TF State Cloud allows sharing the infrastructure state across team members and teams.



## TFstate Cloud vs S3 Buckets
Terraform also supports storing the state over S3 buckets, Why should I choose TF State Cloud instead?
1. **TFstate Cloud is much simpler to setup and use**, it's a single command to setup and configure a Terraform configuration to use TF State Cloud as the state and lock backend:
`curl https://get.tfstate.cloud`.

1. **TFstate Cloud provides better security by default**. The Terraform state files contain sensitive infrastructure information. When storing this information on S3 buckets, anyone who has read access to the buckets can obtain access to critical infrastructure components. Misconfigured S3 bucket permissions are the main source of data leakage of sensitive information.  
TF State Cloud prevents direct access to the state files, and allows Terraform to access the state only via a dedicated `access_secret_key`.

